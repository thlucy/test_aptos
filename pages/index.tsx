import type { NextPage } from 'next'
import React, { useState, useEffect, useCallback } from 'react'
import {
  AptosClient, AptosAccount, FaucetClient, BCS, TxnBuilderTypes,
  TransactionBuilder, HexString,
} from "aptos";
import { Button, Form, Col, Row, Stack } from 'react-bootstrap';

// devnet is used here for testing
const NODE_URL = "https://fullnode.devnet.aptoslabs.com";
const FAUCET_URL = "https://faucet.devnet.aptoslabs.com";

const client = new AptosClient(NODE_URL);
const faucetClient = new FaucetClient(NODE_URL, FAUCET_URL);

function getAccount(secret?: string): AptosAccount {
  const key = secret ? new HexString(secret) : null
  const account = new AptosAccount(key?.toUint8Array())
  return account
}

async function fund(account: AptosAccount, amount: number) {
  return await faucetClient.fundAccount(account.address(), amount);
}

async function getBalance(account: AptosAccount): Promise<any> {
  let resources = await client.getAccountResources(account.address());
  let accountResource = resources.find((r) => r.type === "0x1::coin::CoinStore<0x1::aptos_coin::AptosCoin>");
  return (accountResource?.data as any).coin.value

  // or
  // let resources = await client.getAccountResources(account.address(), "0x1::coin::CoinStore<0x1::aptos_coin::AptosCoin>");
}

async function transfer(accountFrom: AptosAccount, addressTo: HexString, amount: number) {
  const token = new TxnBuilderTypes.TypeTagStruct(TxnBuilderTypes.StructTag.fromString("0x1::aptos_coin::AptosCoin"));

  const scriptFunctionPayload = new TxnBuilderTypes.TransactionPayloadEntryFunction(
    TxnBuilderTypes.EntryFunction.natural(
      "0x1::coin",
      "transfer",
      [token],
      [BCS.bcsToBytes(TxnBuilderTypes.AccountAddress.fromHex(addressTo)), BCS.bcsSerializeUint64(amount)],
    ),
  );

  const [{ sequence_number: sequenceNumber }, chainId] = await Promise.all([
    client.getAccount(accountFrom.address()),
    client.getChainId(),
  ]);

  const rawTxn = new TxnBuilderTypes.RawTransaction(
    TxnBuilderTypes.AccountAddress.fromHex(accountFrom.address()),
    BigInt(sequenceNumber),
    scriptFunctionPayload,
    30n,
    1n,
    BigInt(Math.floor(Date.now() / 1000) + 10),
    new TxnBuilderTypes.ChainId(chainId),
  );

  console.log("raw tx", rawTxn);

  // simulate
  const simTx = new TxnBuilderTypes.SignedTransaction(rawTxn,
    new TxnBuilderTypes.TransactionAuthenticatorEd25519(
      new TxnBuilderTypes.Ed25519PublicKey(accountFrom.pubKey().toUint8Array()),
      new TxnBuilderTypes.Ed25519Signature(new Uint8Array(64)),
    ));
  const txs = await client.submitBCSSimulation(BCS.bcsToBytes(simTx))
  console.log(txs);
  // FIXME use gas_used to set max_gas_amount?

  // sign
  const sigHexStr = accountFrom.signBuffer(TransactionBuilder.getSigningMessage(rawTxn));

  const signTxn = new TxnBuilderTypes.SignedTransaction(rawTxn,
    new TxnBuilderTypes.TransactionAuthenticatorEd25519(
      new TxnBuilderTypes.Ed25519PublicKey(accountFrom.pubKey().toUint8Array()),
      new TxnBuilderTypes.Ed25519Signature(sigHexStr.toUint8Array()),
    ));

  // bin encode
  const bcsTxn = BCS.bcsToBytes(signTxn)
  console.log("sign tx", HexString.fromUint8Array(bcsTxn).hex())

  // sign + bin encode
  const bcsTxn2 = AptosClient.generateBCSTransaction(accountFrom, rawTxn);
  console.log("sign tx 2", HexString.fromUint8Array(bcsTxn2).hex())

  return;
  const pendingTxn = await client.submitSignedBCSTransaction(bcsTxn);
  console.log(pendingTxn)
  return pendingTxn.hash;
}


const KV = ({label, children}: {label: React.ReactNode, children: React.ReactNode}) => {
  return (
    <Row className="mb-3">
      <Col sm="2">{label}</Col>
      <Col sm="10">{children}</Col>
    </Row>
  )
}

const Account = ({name, account}: {name: string, account: AptosAccount}) => {
  const [balance, setBalance] = useState()
  const [fundAmount, setFundAmount] = useState(0)

  useEffect(() => {
    getBalance(account).then((res) => setBalance(res)).catch((err) => console.error(err));
  }, [account])

  return <div>
    <h1>account - {name}</h1>
    <Stack>
      <KV label="address">{account.address().hex()}</KV>
      <KV label="auth key">{account.authKey().hex()}</KV>
      <KV label="pub key">{account.pubKey().hex()}</KV>
      <KV label="secret key">{HexString.ensure(Buffer.from(account.signingKey.secretKey).toString("hex")).hex()}</KV>
      <KV label="balance">{balance}</KV>
    </Stack>
    <Row className="mb-3">
      <Col sm="2">
        <Button onClick={() => fund(account, fundAmount)}>Fund</Button>
      </Col>
      <Col sm="3">
        <Form.Group as={Col}>
          <Form.Label>amount</Form.Label>
          <Form.Control type="number" value={fundAmount} onChange={(e) => setFundAmount(parseInt(e.target.value, 10))} />
        </Form.Group>
      </Col>
    </Row>
    <TransferNativeToken from={account} />
  </div>
}

const TransferNativeToken = ({from}: {from:AptosAccount}) => {
  const [amount, setAmount] = useState(10);
  const [to, setTo] = useState<HexString>();

  return <>
    <Form onSubmit={(e) => {
      e.stopPropagation()
      e.preventDefault()
      if (to == null) {
        return
      }
      transfer(from, to, amount);
      }}>
      <Row className="mb-3">
        <Col sm="2"><Button type="submit">Transfer</Button></Col>
        <Form.Group as={Col}>
          <Form.Label>to</Form.Label>
          <Form.Control value={to?.hex()} onChange={(e) => { setTo(HexString.ensure(e.target.value)) }}/>
        </Form.Group>
        <Form.Group as={Col}>
          <Form.Label>amount</Form.Label>
          <Form.Control type="number" value={amount} onChange={(e) => setAmount(parseInt(e.target.value, 10))} />
        </Form.Group>
      </Row>
    </Form>
  </>
}

const BlockInfo = () => {
  const [height, setHeight] = useState<number>()
  const [block, setBlock] = useState<any>()

  useEffect(() => {
    client.getLedgerInfo()
      .then(res => {
        console.log("ledger", res);
        setHeight(parseInt(res.block_height, 10));
      })
  }, [])

  return <>
    <h1>block</h1>
    <Form onSubmit={(e) => {
      e.stopPropagation()
      e.preventDefault()
      if (height == null) {
        return
      }
      client.client.blocks.getBlockByHeight(height, true).then(blk => setBlock(blk))
      }}>
      <Row className="my-3">
        <Col sm="2"><Button type="submit">Get Block</Button></Col>
        <Form.Group as={Col}>
          <Form.Label>height</Form.Label>
          <Form.Control type="number" value={height} onChange={(e) => setHeight(parseInt(e.target.value, 10))} />
        </Form.Group>
      </Row>
    </Form>
    <pre>
      {JSON.stringify(block, undefined, "  ")}
    </pre>
  </>
}

const TransactionInfo = () => {
  const [version, setVersion] = useState<BigInt>()
  const [tx, setTx] = useState<any>()

  return <>
    <h1>transaction</h1>
    <Form onSubmit={(e) => {
      e.stopPropagation()
      e.preventDefault()
      if (version == null) {
        return
      }
      client.getTransactionByVersion(version).then(res => setTx(res))
      }}>
      <Row className="my-3">
        <Col sm="2"><Button type="submit">Get Transaction</Button></Col>
        <Form.Group as={Col}>
          <Form.Label>version</Form.Label>
          <Form.Control type="number" value={version?.toString()} onChange={(e) => setVersion(BigInt(e.target.value))} />
        </Form.Group>
      </Row>
    </Form>
    <pre>
      {JSON.stringify(tx, undefined, "  ")}
    </pre>
  </>
}

const Home: NextPage = () => {
  const acc1 = getAccount("0x34b17e1faa6921684a253df8f66655180bdd461d9541677e71747a8361f3e667ce1c3c51b008dc7784b2c62e3cb496fb2aa1585606c1f6bc9538e45d0efe2df3")
  const acc2 = getAccount("0x5b5bac65ee0e21f8780cfd8ce3b7bd998379efd3e60c5fac5a6e92fc87a10bbe68ee1ec0b420a006ffb6df2a856daadc3ad3d24321ab6f9ba65df8690a81d026")

  useEffect(() => { (window as any).client = client; }, [])

  return (
    <div className="mx-3">
      <Account name="alice" account={acc1} />
      <Account name="bob" account={acc2} />
      <BlockInfo />
      <TransactionInfo />
    </div>
  )
}

export default Home
